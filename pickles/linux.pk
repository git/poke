/* linux.pk - Linux kernel binary data structures.  */

/* Copyright (C) 2023, 2024, 2025 Jose E. Marchesi */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

type Linux_Version_Code =
  struct uint<32>
  {
    uint<8>;
    uint<8> major;
    uint<8> middle;
    uint<8> minor;

    method _print = void:
    {
      printf "#<%u8d.%u8d.%u8d>", major, middle, minor;
    }
  };
