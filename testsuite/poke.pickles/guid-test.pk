/* guid-test.pk - Tests for the guid pickle.  */

/* Copyright (C) 2023 Denis Maryin */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

load pktest;
load guid;

var GUID_DATA = [
  0x00UB, 0x11UB, 0x22UB, 0x33UB, 0x44UB, 0x55UB, 0x66UB, 0x77UB,
  0x88UB, 0x99UB, 0xaaUB, 0xbbUB, 0xccUB, 0xddUB, 0xeeUB, 0xffUB
];

var EXPECTED_GUID = GUID {
  data1=0x33221100U, data2=0x5544UH, data3=0x7766UH,
  data4=[0x88UB, 0x99UB, 0xaaUB, 0xbbUB, 0xccUB, 0xddUB, 0xeeUB, 0xffUB]
};

var tests = [
  PkTest {
    name = "invariant on both endiannesses",
    func = lambda (string name) void:
      {
        with_temp_ios
          :endian ENDIAN_BIG
          :do lambda void:
            {
              uint<8>[16] @ 0#B = GUID_DATA;
              var present = GUID @ 0#B;
              assert(EXPECTED_GUID == present);
            };
        with_temp_ios
          :endian ENDIAN_LITTLE
          :do lambda void:
            {
              uint<8>[16] @ 0#B = GUID_DATA;
              var present = GUID @ 0#B;
              assert(EXPECTED_GUID == present);
            };
      },
  },
];

var ec = pktest_run (tests) ? 0 : 1;

exit (ec);
